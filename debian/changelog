libspreadsheet-read-perl (0.91-1) unstable; urgency=medium

  * Import upstream version 0.91.
  * Update long description.
  * Update test and runtime dependencies.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Sun, 08 Sep 2024 02:28:13 +0200

libspreadsheet-read-perl (0.90-2) unstable; urgency=medium

  * debian/control: add libexcel-valuereader-xlsx-perl.
  * Add empty debian/tests/pkg-perl/syntax-skip for more autopkgtests.

 -- gregor herrmann <gregoa@debian.org>  Wed, 22 May 2024 18:48:22 +0200

libspreadsheet-read-perl (0.90-1) unstable; urgency=medium

  * Import upstream version 0.90.

 -- gregor herrmann <gregoa@debian.org>  Sun, 18 Feb 2024 00:32:33 +0100

libspreadsheet-read-perl (0.89-1) unstable; urgency=medium

  * Import upstream version 0.89.
  * Update years of upstream and packaging copyright.
  * Make some packages in Build-Depends-Indep and Recommends versioned
    to ensure newest/fixed versions.

 -- gregor herrmann <gregoa@debian.org>  Sun, 07 Jan 2024 22:30:26 +0100

libspreadsheet-read-perl (0.88-1) unstable; urgency=medium

  * Import upstream version 0.88.
  * Refresh Build-Depends-Indep, Depends, Recommends, and Suggests.

 -- gregor herrmann <gregoa@debian.org>  Sat, 11 Nov 2023 17:58:57 +0100

libspreadsheet-read-perl (0.87-1) unstable; urgency=medium

  * Import upstream version 0.86.
  * Import upstream version 0.87.
  * debian/rules: drop overrides which fixed hashbangs.

 -- gregor herrmann <gregoa@debian.org>  Fri, 24 Feb 2023 04:41:20 +0100

libspreadsheet-read-perl (0.85-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libtext-csv-perl.
    + libspreadsheet-read-perl: Drop versioned constraint on libtext-csv-perl
      in Recommends.

  [ gregor herrmann ]
  * Import upstream version 0.85.
  * Update years of upstream and packaging copyright.
  * Bump versioned (test) dependency on libdata-peek-perl.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Sat, 07 Jan 2023 19:06:18 +0100

libspreadsheet-read-perl (0.84-1) unstable; urgency=medium

  * Import upstream version 0.84.
  * Update years of upstream and packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 28 Feb 2021 03:10:33 +0100

libspreadsheet-read-perl (0.83-1) unstable; urgency=medium

  * Import upstream version 0.83.
  * Update debian/libspreadsheet-read-perl.examples (renamed directory).
  * Bump required libspreadsheet-readsxc-perl version to 0.26.
  * Declare compliance with Debian Policy 4.5.1.

 -- gregor herrmann <gregoa@debian.org>  Tue, 22 Dec 2020 02:17:57 +0100

libspreadsheet-read-perl (0.82-1) unstable; urgency=medium

  * Import upstream version 0.82.
  * Update years of upstream and packaging copyright.
  * Update build and runtime dependencies.
  * Update long description.
  * Drop 0001-We-still-don-t-support-field-attributes.patch.
    The patched file is gone, and the fix shouldn't be required anymore.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository.

 -- gregor herrmann <gregoa@debian.org>  Fri, 23 Oct 2020 23:19:25 +0200

libspreadsheet-read-perl (0.81-1) unstable; urgency=medium

  * Import upstream version 0.81.
  * Update years of upstream copyright.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.1.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.
  * Add patch from upstream Git repo to disable failing test with
    Spreadsheet::ReadSXC.

 -- gregor herrmann <gregoa@debian.org>  Sat, 12 Oct 2019 20:54:36 +0200

libspreadsheet-read-perl (0.80-1) unstable; urgency=medium

  * Import upstream version 0.80.
  * debian/control: replace the deprecated libspreadsheet-xlsx-perl with
    libspreadsheet-parsexlsx-perl. Update long description accordingly.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.3.0.
  * Bump debhelper compatibility level to 11.
  * Remove trailing whitespace from debian/*.

 -- gregor herrmann <gregoa@debian.org>  Sat, 05 Jan 2019 00:20:12 +0100

libspreadsheet-read-perl (0.79-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.79.
  * Bump versioned (build) dependency on libdata-peek-perl.
  * Declare compliance with Debian Policy 4.2.1.

 -- gregor herrmann <gregoa@debian.org>  Fri, 21 Sep 2018 19:37:22 +0200

libspreadsheet-read-perl (0.78-1) unstable; urgency=medium

  * Import upstream version 0.78.

 -- gregor herrmann <gregoa@debian.org>  Mon, 19 Feb 2018 20:37:42 +0100

libspreadsheet-read-perl (0.76-1) unstable; urgency=medium

  * Import upstream version 0.76.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.1.3.
  * Bump debhelper compatibility level to 10.
  * Add libspreadsheet-readsxc-perl to Recommends and Build-Depends-Indep.
    (Closes: #884867)
  * Update long description: mention libspreadsheet-readsxc-perl.

 -- gregor herrmann <gregoa@debian.org>  Sun, 07 Jan 2018 00:27:57 +0100

libspreadsheet-read-perl (0.75-1) unstable; urgency=medium

  * Import upstream version 0.75.
  * Update years of upstream and packaging copyright.
  * Bump versioned (build) dependency on libdata-peek-perl.
  * Declare compliance with Debian Policy 4.1.1.

 -- gregor herrmann <gregoa@debian.org>  Wed, 15 Nov 2017 19:53:52 +0100

libspreadsheet-read-perl (0.69-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Jose Luis Rivas from Uploaders. Thanks for your work!
  * Remove Ryan Niebur from Uploaders. Thanks for your work!
  * Remove Jeremiah C. Foster from Uploaders on request of the MIA team.
    (Closes: #843746)

  [ Lucas Kanashiro ]
  * Import upstream version 0.69
  * debian/control: remove explicitly build dependency on versioned Test::More,
    requires version 0.88 that is satisfied in all maintained Debian
    distributions

 -- Lucas Kanashiro <kanashiro@debian.org>  Thu, 22 Dec 2016 15:23:31 -0200

libspreadsheet-read-perl (0.66-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Lucas Kanashiro ]
  * Import upstream version 0.66
  * debian/control: update version of libdata-peek-perl

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Fri, 17 Jun 2016 14:18:49 -0300

libspreadsheet-read-perl (0.64-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Lucas Kanashiro ]
  * Import upstream version 0.64
  * Update years of upstream copyright
  * Update Debian packaging copyright
  * Declare compliance with Debian policy 3.9.8
  * debian/control: update dependencies

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Mon, 25 Apr 2016 18:21:00 -0300

libspreadsheet-read-perl (0.63-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * New upstream release.
  * Add debian/upstream/metadata.
  * Replace patch with override in debian/rules.
  * Update years of upstream and packaging copyright.
  * Make (build) dependency on libdata-peek-perl versioned.
  * Install new CONTRIBUTING.md file.
  * Remove version from libspreadsheet-xlsx-perl in Depends.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Tue, 27 Oct 2015 22:13:05 +0100

libspreadsheet-read-perl (0.54-1) unstable; urgency=low

  [ Nicholas Bamber ]
  * New upstream release 0.45
  * Raised standards version to 3.9.2
  * Removed redundant version dependencies
  * Updated copyright

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * New upstream release 0.46.
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team.
  * Strip trailing slash from metacpan URLs.

  * New upstream release 0.54.
  * Update build and runtime dependencies.
  * debian/rules: set AUTOMATED_TESTING=1 for dh_auto_configure. This
    avoids installation of the example scripts and a generated symlink.
  * Update years of copyright.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Mon, 05 May 2014 18:12:47 +0200

libspreadsheet-read-perl (0.41-1) unstable; urgency=low

  [ Nicholas Bamber ]
  * Added myself to Uploaders
  * New upstream release
  * Upped standards version to 3.9.1
  * Refreshed copyright
  * Refreshed interpreter patch and removed spelling patch

  [ gregor herrmann ]
  * debian/control: bump version for some (build) dependencies according to
    new upstream requirements.

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Tue, 12 Oct 2010 20:27:19 +0100

libspreadsheet-read-perl (0.40-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release

  [ gregor herrmann ]
  * Update (build) dependency versions as per upstream.
  * Update DEP3 patch headers.

 -- gregor herrmann <gregoa@debian.org>  Fri, 21 May 2010 17:10:51 +0200

libspreadsheet-read-perl (0.39-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Standards-Version 3.8.4 (no changes)
  * Use new 3.0 (quilt) source format
  * Update to DEP5 copyright format
    + Update years of copyright
  * Replace patch with override to fix shebang lines

  [ gregor herrmann ]
  * Bump some versioned dependencies according to new upstream requirements.
  * Add a patch to fix a spelling mistake in the POD.
  * Resurrect the part of interpreter.patch that does not act on the examples.

 -- gregor herrmann <gregoa@debian.org>  Sun, 28 Mar 2010 17:24:00 +0200

libspreadsheet-read-perl (0.38-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Add row() and cellrow()

  [ gregor herrmann ]
  * debian/control: bump some versioned (build) dependencies as per new
    upstream requirements.

 -- Jonathan Yu <jawnsy@cpan.org>  Tue, 15 Dec 2009 20:26:59 -0500

libspreadsheet-read-perl (0.37-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Update dependencies per upstream
  * Standards-Version 3.8.3 (drop perl version dependency)
  * Rewrite control description
  * Add myself to Uploaders and Copyright

  [ Ryan Niebur ]
  * Update ryan52's email address

 -- Jonathan Yu <jawnsy@cpan.org>  Tue, 10 Nov 2009 08:58:00 -0500

libspreadsheet-read-perl (0.35-1) unstable; urgency=low

  [ Ryan Niebur ]
  * New upstream release (closes: #526567)
  * Add myself to Uploaders
  * Debian Policy 3.8.1
  * Add build dep on libtest-nowarnings-perl

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * Minimize debian/rules, bump versioned build dependencies; activate
    AUTOMATED_TESTING.
  * Set Standards-Version to 3.8.2 (no changes).
  * Refresh patch.
  * Add a build dependency on libtest-simple-perl >= 0.88, the test suite now
    uses a feature in a newer Test::More than the one included in perl-modules.
  * Bump versioned (build) dependencies on several prerequisites.

 -- gregor herrmann <gregoa@debian.org>  Fri, 26 Jun 2009 17:52:18 +0200

libspreadsheet-read-perl (0.34-1) unstable; urgency=low

  * New upstream release.
  * debian/control: bump versioned (build) dependencies.
  * Add padding 0s to the required versions of libspreadsheet-parseexcel-perl,
    otherwise 0.3300 >> 0.49.

 -- gregor herrmann <gregoa@debian.org>  Thu, 29 Jan 2009 19:45:19 +0100

libspreadsheet-read-perl (0.33-1) unstable; urgency=low

  * New upstream release.
  * debian/control: bump versioned (build) dependencies and add
    libspreadsheet-xlsx-perl.
  * Extend interpreter.patch to include new example script.
  * debian/control: update short and long description.

 -- gregor herrmann <gregoa@debian.org>  Sat, 24 Jan 2009 03:47:50 +0100

libspreadsheet-read-perl (0.31-1) unstable; urgency=low

  * New upstream release.
  * Extend interpreter.patch to also fix the path in the examples.
  * Update years of upstream copyright.

 -- gregor herrmann <gregoa@debian.org>  Mon, 05 Jan 2009 14:52:40 +0100

libspreadsheet-read-perl (0.30-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - switch Vcs-Browser field to ViewSVN
    - bump versioned (build) dependencies on libspreadsheet-parseexcel-perl
      and libtext-csv-xs-perl
    - remove module versions from long description
    - add /me to Uploaders
  * Add a description to interpreter.patch.

 -- gregor herrmann <gregoa@debian.org>  Thu, 01 Jan 2009 22:35:24 +0100

libspreadsheet-read-perl (0.29-1) unstable; urgency=low

  [ Jose Luis Rivas ]
  * New upstream release
  * Added me as uploader.

  [ gregor herrmann ]
  * debian/control:
    - bump versioned Recommends on libtext-csv-xs-perl to 0.56
    - make build dependencies versioned too
    - wrap a long line

 -- Jose Luis Rivas <ghostbar38@gmail.com>  Sun, 02 Nov 2008 03:37:27 -0430

libspreadsheet-read-perl (0.28-1) unstable; urgency=low

  * Initial Release. Closes: #497780

 -- Jeremiah C. Foster <jeremiah@jeremiahfoster.com>  Tue, 23 Sep 2008 22:25:11 +0200
